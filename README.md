## 1. Configurar WindowManager

El proyecto necesita de los permisos de Accesibilidad por lo que se tiene que crear una clase que extienda a `AccessibilityService` (no olvidar registrarlo en el archivo `AndroidManifest.xml`). Luego se debe anular el metodo `onCreate` y otorgar el WindowManager como el siguiente ejemplo

```java
public class SomeService extends AccessibilityService {
    public static volatile WindowManager windowManager;
    @Override
    public void onCreate() {
        super.onCreate();
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Widget.setWindowManager(windowManager);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {}

    @Override
    public void onInterrupt() {}
}
```

## 2. Registrar el Receiver

```java
WidgetReceiver.register(context);
```

## 3. Dar de baja al Receiver

```java
WidgetReceiver.unregister(context);
```

## 4. Llamar al widget deseado

```java
Intent intent = new Intent(StartBottomWidgetCommand.START_BOTTOM_WIDGET);
intent.putExtra(Widget.style, WidgetMPOSImplementation.MPOS_WIDGET);
sendBroadcast(intent);
```

## Lista de Actions

| Actions                                     | Descripción                                                      |
|:------------------------------------------- | ---------------------------------------------------------------- |
| `WidgetStyle.FULL_SCREEN_WITH_TIMER.name()` | Lanza una ventana del tamaño de la pantalla del dispositivo      |
| `WidgetStyle.NAVIGATION_BAR.name()`         | Lanza una ventana que cubre la barra de navegación               |
| `WidgetStyle.POWER_MENU.name()`             | Lanza una ventana con tamaño personalizado (a través de un JSON) |
| `WidgetStyle.CALL_RTC.name()`               | Lanza una ventana que cubre la barra de navegación               |
| `WidgetCommand.CLOSE_WIDGET`                | Cierra la ventana que se este mostrando                          |

> Estos intents tambien permiten reposicionar la ventana. Por ejemplo: pasar de modo horizontal a vertical.

## Intent START_CUSTOM_WIDGET

El JSON que se debe proporcionar debe tener la siguiente estructura

```json
{
    "x": 0,
    "y": 0,
    "height": 800,
    "width": 400
}
```

Luego se debe enviar el JSON como cadena de la siguiente forma

```java
String jsonWidget ="{'x': 0, 'y' : 0, 'height' : 800, 'width' : 400}";
Intent intent = new Intent(StartCustomWidgetCommand.START_CUSTOM_WIDGET):
intent.putExtra(Widget.style, WidgetMPOSImplementation.MPOS_WIDGET);
intent.putExtra(WidgetDTO.JSON, jsonWidget);
sendBroadcast(intent);
```

## Lista de Extras

| Nombre                                 | Descripción                                                                                           |
| -------------------------------------- | ----------------------------------------------------------------------------------------------------- |
| `WidgetMPOSImplementation.MPOS_WIDGET` | El layout que lanzara si se usa este atributo es uno vacio.                                           |
| `WidgetGPSImplementation.GPS_WIDGET`   | El layout que lanzara si se usa este atributo es uno que contiene un botón de cierre y un cronómetro. |

> `Widget.style` es la clave con la que se identifica a la implementación que se esta usando
> 
> `WidgetDTO.JSON` es la clave con la que se identifica al JSON que se envia.
