package com.abexacloud.widget;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.abexacloud.widget_library.widget.Widget;
import com.abexacloud.widget_library.widget.WidgetCommand;
import com.abexacloud.widget_library.widget.WidgetReceiver;
import com.abexacloud.widget_library.widget.WidgetUtil;
import com.abexacloud.widget_library.widget.WidgetStyle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final int h = WidgetUtil.dpToPixels(130, getApplicationContext());
        final int w = WidgetUtil.dpToPixels(90, getApplicationContext());
        final String jsonWidget = "{'x': 0, 'y' : 0, 'height' :" + h + ", 'width' :" + w + "}";

        setContentView(R.layout.activity_main);
        Button buttonStart = findViewById(R.id.buttonStart);

        WidgetReceiver.register(this);

        Button buttonPermission = findViewById(R.id.buttonPermission);
        buttonPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
            }
        });

        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WidgetStyle.FULL_SCREEN_WITH_TIMER.name());
//                intent.putExtra(WidgetDTO.JSON, jsonWidget);
                sendBroadcast(intent);
            }
        });

        Button buttonUpdateParams = findViewById(R.id.buttonUpdateParams);
        buttonUpdateParams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int i = 1; i <= 10; i++) {
                    Intent intent = new Intent(WidgetStyle.CALL_RTC.name());
                    intent.putExtra("timer", i);
                    sendBroadcast(intent);
                }
            }
        });

        Button buttonClose = findViewById(R.id.buttonClose);
        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WidgetCommand.CLOSE_WIDGET.name());
                intent.putExtra(Widget.style, WidgetStyle.FULL_SCREEN_WITH_TIMER);
                sendBroadcast(intent);
            }
        });
    }
}