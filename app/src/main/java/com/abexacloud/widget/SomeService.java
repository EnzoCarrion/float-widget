package com.abexacloud.widget;

import android.accessibilityservice.AccessibilityService;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;

import com.abexacloud.widget_library.widget.Widget;

public class SomeService extends AccessibilityService {
    public static volatile WindowManager windowManager;

    @Override
    public void onCreate() {
        super.onCreate();
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Widget.setWindowManager(windowManager);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {

    }

    @Override
    public void onInterrupt() {

    }
}
